import fetch from 'node-fetch';
import * as fs from 'fs-extra';
import * as path from 'path';
import MarkDownDOM from '../src/dom';

void async function () {
  try {
    let counter = 0;

    /* Test files */
    const files = (await fs.readdir('../data')).filter(file => file.endsWith('.md'));
    for (const file of files) {
      const target = String(await fs.readFile(path.join('../data/', file)));
      console.log(`${counter}/${files.length}: ${file}`);
      if (await test(target)) {
        return;
      }
    }

    counter++;

    /* NPM packages */
    let data;
    if (!await fs.pathExists('cache.json')) {
      const response = await fetch('https://skimdb.npmjs.com/registry/_all_docs');
      data = await response.json();
      await fs.writeJson('cache.json', data, { replacer: null, spaces: 2 });
    } else {
      data = await fs.readJson('cache.json');
    }

    counter = 0;
    for (const row of data.rows) {
      let item;
      if (!await fs.pathExists(`cache/${row.id}.json`)) {
        const response = await fetch('https://skimdb.npmjs.com/registry/' + row.id);
        item = await response.json();
        await fs.writeJson(`cache/${row.id}.json`, item, { replacer: null, spaces: 2 });
      } else {
        item = await fs.readJson(`cache/${row.id}.json`);
      }

      if (item === undefined || item.error) {
        // Probably a removed package?
        continue;
      } else {
        console.log(`${counter}/${data.total_rows}: ${item.name}`);
      }

      const target = item.readme;
      if (await test(target)) {
        return;
      }

      counter++;
    }
  } catch (error) {
    console.log(error);
  }
}()

async function test(target: string) {
  let parsed: MarkDownDOM;
  let actual: string;
  try {
    parsed = MarkDownDOM.parse(target);
    actual = parsed.toString();

    if (actual !== target) {
      await fail(parsed, actual, target);
      return false;
    }
  } catch (error) {
    console.log('ERROR!', error.message, error.stack);
    await fail(parsed, actual, target);
    return false;
  }
}

async function fail(parsed, actual, target) {
  await fs.writeFile('../data/_npm.md', target);
  console.log('\tParsed:', JSON.stringify(parsed));

  let index: number;
  for (let i = 0; i < target.length; i++) {
    if (actual[i] !== target[i]) {
      index = i;
      console.log('\tIndex:', index);
      console.log('\tActual character:', JSON.stringify(actual[index]));
      console.log('\tTarget character:', JSON.stringify(target[index]));
      break;
    }
  }

  console.log('\tActual:', JSON.stringify(actual.substring(index)));
  console.log('\tTarget:', JSON.stringify(target.substring(index)));
}
