# Heading 1

##

not a heading or is it?

## Headers
do not break like this

But paragraphs
do!

   ## Heading 2

The above one will look like a paragraph for a bit, but will be confirmed to be a header after three spaces.

## Heading 3 - Inline *italics* and **bold** text

[link](https://google.com) and [`link` with *style*](https://google.com)

-un
- or
- de
- red
- list

-is
-this
-still
-a list? no!

 - space
- padded
 - unordered
- list

	- tab
	- pad

10. ord
1. er
1. ed
1. list

Paragraph *1* is **here**
and it is split into two lines.

Paragraph **2** is *here*
and it is
split
into
3
lines.

This paragraph is not split into any lines. :-(

`this is a code span`

```js
alert("And a fended code block");
```

# Heading 1 again

```html bloggo
<script>
console.log('This will be embedded in the resulting HTML.');
</script>
```

			 #
       ^ up until this point, the above block is treated as a paragraph
