issue: [**bold** & _italic_, **_bitalic_**](about:blank) or just [link][ref] [self-ref]

_**bitalic-wrong_**

[*bold](lol)*

ref: https://google.com/ncr
self-ref: https://google.com/ncr

In this example, the parser should know that the `[` might become a link (but it could also be a checkbox in a list or a reference link).
Only when `](…)` is fully found it should go back and rebuild the DOM to make this a link, because until then it's not a full link.
We cannot assume that until it's set in stone, otherwise parsing and stringifying would result in non-equal outputs.
