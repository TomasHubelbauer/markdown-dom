# MarkDown DOM

> A MarkDown deserializer/serializer which provides a DOM for manipulation MarkDown documents.

**Examples:**

```md
# Header

## Headers
do not break line this

But paragraphs
do!

```

```json
{
  "blocks": [
    { "type": "header", "level": 1, "spans": [ { "type": "run", "text": " Header" } ] },
    { "type": "paragraph", "spans": [ { "type": "run", "text": "" } ] },
    { "type": "header", "level": 2, "spans": [ { "type": "run", "text": "Headers" } ] },
    { "type": "paragraph", "spans": [ { "type": "run", "text": "do not break like this" } ] },
    { "type": "paragraph", "spans": [ { "type": "run", "text": "But paragraphs" }, { "type": "newline" }, { "type": "do!" } ] }
  ]
}
```

## Running

Open VS Code integrated terminal and run:

```sh
cd demo
yarn
yarn start
```

## Testing

Open VS Code integrated terminal and run:

```sh
cd test
yarn start
```

## Publishing

- Bump version and changelog
- `npm whoami`
- `yarn build`
- `npm publish`

## Debugging

It is recommended to open a new terminal session for each `yarn start` run to have only that run's logs in the scrollback.
