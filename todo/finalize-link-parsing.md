# Finalize link parsing

Travel the blocks and depending on their type, their plain text contents to find link inlines.

Have a test to make sure this finds the same links the following regex used in MarkDown Link Suggestions does:

[`/(?:__|[*#])|\[(.*?)\]\((.*?)\)/gm`](https://github.com/TomasHubelbauer/vscode-markdown-link-suggestions/blob/master/src/extension.ts#L300)
