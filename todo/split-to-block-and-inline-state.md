# Split to block and inline state

We might need to remember some info about the block and have that info be preserved even as we enter the inlines in the block.

Example: when parsing a block (like a paragraph or a list), we need to remember which `[` are link candidates until they are confirmed links.
But also the link text may have further parsing in it for inlines (like `[*bold* & _italic_](about:blank)`).
