# Split test into pluggable sources

Sources:

- Files from `data`
- READMEs of my GitLab repositories (for Bloggo)
- NPM package READMEs
