# Tasks

> Development plan

## Planned

### Use these Daring Fireball links for inspiration:

- https://daringfireball.net/projects/markdown/syntax
- https://daringfireball.net/projects/markdown/dingus

### Get rid of ambiguous block, block is considered a paragraph until proven otherwise

### Fix *ReferenceError: HEADER_BLOCK_TYPE is not defined* TS fuckery

With `yarn start`.

### Support unordered lists with padding

Allow for padding, spaces at the start of a block should keep it in a paragraph state with a run of the spaces
until a disambiguator like `-`, `#.` or ` is found.

### Support ordered lists with padding

Allow for padding, spaces at the start of a block should keep it in a paragraph state with a run of the spaces
until a disambiguator like `-`, `#.` or ` is found.

### Support fenced code blocks with padding

Allow for padding, spaces at the start of a block should keep it in a paragraph state with a run of the spaces
until a disambiguator like `-`, `#.` or ` is found.

### Support inline constructs in a way that reuses the states

Should be possible to jump from heading block state to inline parsing state and then back to heading block state
without having flows like this:

- `heading` -> `heading-spans` -> `heading`
- `paragraph` -> `paragraph-spans` -> `paragraph`

It should be smart like this:

`heading`/`paragraph` -> `inline-spans` (knows to go back to block type that started it) -> the initial block type

### Support `\r\n`

Currently buiding with just `\n` in mind.

## Backlog

### Consider a toggle for choosing what depth of headings to support at most

Anything beyond that will be treated as a paragraph block.

### Support `bloggo` as the second tag of the `html`, `css` and `js` fenced code blocks that would embed verbatim in the output

Depending on language would either drop verbatim HTML or what in `script` or `style`.
