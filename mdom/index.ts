import Paragraph from "./Paragraph";
import Header from "./Header";
import Table from "./Table";

export default class Document {
  private blocks: (Paragraph | Header | Table)[] = [];

  public constructor(markdown: string) {
    for (let index = 0; index < markdown.length; index++) {
      const character = markdown[index];
      switch (character) {
        case '\n': {
          const block = this.blocks[this.blocks.length - 1];
          if (block !== undefined) {
            block.parse(character);
          } else {
            const block = new Paragraph();
            this.blocks.push(block);
          }

          break;
        }
        case '#': {
          this.blocks.push(new Header());
          break;
        }
        default: {
          let block = this.blocks[this.blocks.length - 1];
          if (block === undefined) {
            block = new Paragraph();
            this.blocks.push(block);
          }

          block.parse(character);
        }
      }
    }
  }
}

console.log(require('util').inspect(new Document(`
---
metadata: true
---

# Header

Here's a link: [link 1](README.md)
You can click it

[link 2](./dir/dir2/dir3/file.ext?query#fragment)

## More links

Back to [top](#header)

[
  this is also a working link
](target "title")

`), { depth: 10, colors: true }));
