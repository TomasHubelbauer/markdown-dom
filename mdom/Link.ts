export default class Link {
  private text = '';
  private url = '';
  private transitioned: 'no' | 'transitioning' | 'yes' = 'no';

  public parse(character: string) {
    switch (character) {
      case ']': {
        if (this.transitioned !== 'no') {
          throw new Error();
        }

        this.transitioned = 'transitioning';
        break;
      }
      case '(': {
        if (this.transitioned !== 'transitioning') {
          throw new Error();
        }

        this.transitioned = 'yes';
        break;
      }
      case ')': {
        if (this.transitioned !== 'yes') {
          throw new Error();
        }

        break;
      }
      default: {
        switch (this.transitioned) {
          case 'no': this.text += character; break;
          case 'transitioning': throw new Error();
          case 'yes': this.url += character; break;
          default: throw new Error();
        }
      }
    }
  }
}
