import Link from "./Link";
import Text from "./Text";

export default class Paragraph {
  private spans: (Text | Link)[] = [];
  private latestSpanParse: Parse | undefined;
  private lineBreak: '\n' | '\r\n' | undefined;

  public parse(character: string) {
    switch (character) {
      case '\n': {
        this.lineBreak = '\n';
      }
      case '[': {
        const anchor = new Link();
        this.spans.push(anchor);
        return anchor;
      }
      case '#': {
        throw new Error();
      }
      default: {
        const span = this.spans[this.spans.length - 1];
        if (span === undefined) {
          const span = new Text(this.assignLatestSpanParse);
          this.spans.push(span);
          this.latestSpanParse!(character);
        }

        if (span instanceof Link) {

        }

        throw new Error();
      }
    }
  }

  private assignLatestSpanParse: ParseCallback = parse => this.latestSpanParse = parse;
}
