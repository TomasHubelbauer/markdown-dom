type Parse = (character: string) => void);
type ParseCallback = (parse: Parse) => void;
