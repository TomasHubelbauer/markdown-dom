import Link from "./Link";
import Text from "./Text";

export default class Header {
  private readonly spans: (Link)[] = [];
  private level = 1;
  private whiteSpace = '';
  private transitioned = false; // From white space to text
  private lineBreak: '\n' | '\n\r' | undefined;

  public parse(character: string) {
    switch (character) {
      case '#': {
        throw new Error();
      }
      case ' ': {
        if (this.transitioned) {
          //this.text += character;
        } else {
          this.whiteSpace += character;
        }

        break;
      }
      case '\n': {
        this.lineBreak = '\n';
        break;
      }
      default: {
        const span = this.spans[this.spans.length - 1];
        if (span === undefined) {

        }

        if (span instanceof Text) {

        }

        throw new Error();
      }
    }
  }
}
