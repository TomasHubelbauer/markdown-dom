# MarkDown DOM

This is a new spike.

There is an idea to improve it further and keep the amount of branches in each class down
whereby for example the header wouldn't be a single class but an amalgation of HeaderHash,
WhiteSpace and the array of spans chained together, each parsing their own logic.

Nested parsing, for formatting etc, should be simple with the pattern of consuming what
is known and upon hittin an unknown character deferring to the parent.

A better distinction should be made between the blocks and the spans, maybe two interfaces
or something like in the big MarkDown DOM where the type system would be utilized for it
with support from static properties for types.

## Running

`npm start`

# Contributing

> This is more of a to-do list for me / a roadmap for you.

- See if the parse callback pattern has any merit
  - It would nicely hide `parse` in the resulting DOM so that the returned tree would be immutable
  - It has a drawback of not serving the case where we want to revert something, like `[ ]` to a checkbox
