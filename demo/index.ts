import * as express from 'express';
import * as fs from 'fs-extra';
import * as jsondiff from 'diff-json-structure'; // Is based on `diff`
import * as textdiff from 'diff';
import MarkDownDOM from '../src/dom';
import * as path from 'path';

type Change = { added: boolean; removed: boolean; value: string; };

const server = express();
const testDirectoryPath = '../data';
const port = 3002;

server.get('/', async (request, response) => {
	const documentFilePaths = (await fs.readdir(testDirectoryPath)).filter(documentFilePath => documentFilePath.endsWith('.md'));
	let html = '<title>MarkDownDOM</title><h1>MarkDownDOM</h1>';
	for (const documentFilePath of documentFilePaths) {
		html += `<li><a href="/${documentFilePath}">${documentFilePath} (${(await fs.stat(path.join(testDirectoryPath, documentFilePath))).size})</a></li>`;
	}

	response.send(html);
});

server.get('/:documentFilePath', async (request, response) => {
	const documentFilePath = request.params['documentFilePath'];
	const markdown = String(await fs.readFile(path.join(testDirectoryPath, documentFilePath)));
	let baseline = parse('');
	let html = `<title>${documentFilePath}</title><h1>${documentFilePath} (${markdown.length})</h1>`;
	for (let index = 1; index < markdown.length; index++) {
		const state = parse(markdown.substring(0, index - 1));
		const sourceOriginal = markdown.substring(0, index);
		let sourceReconstructed;
		try {
			sourceReconstructed = parse(markdown.substring(0, index)).toString();
		} catch (error) {
			sourceReconstructed = error.stack;
		}

		const reconstructionError = sourceOriginal !== sourceReconstructed;
		const sourceDiff = print(textdiff.diffLines(sourceOriginal, sourceReconstructed), false);
		const sourceRest = `<pre>${markdown.substring(index)}</pre>`;
		const stateDiff = print(jsondiff(baseline, state), true);
		html += `<details${reconstructionError ? ' open' : ''}>`;
		html += `<summary><h2 style="display: inline;">${index - 1} -> ${index}: ${JSON.stringify(markdown[index - 2] || '')} -> ${JSON.stringify(markdown[index - 1])}</h2></summary>`;
		html += '<h3>Source Diff</h3>';
		html += sourceDiff;
		html += '<h3>Source Rest</h3>';
		html += sourceRest;
		html += '<h3>State Diff</h3>';
		html += stateDiff;
		html += `</details>`;
		baseline = state;

		if (reconstructionError) {
			html += `<p>Reconstruction error.</p>`;
			break;
		}
	}

	response.send(html);
});

server.listen(port, () => console.log(`Listening… localhost:${port}`));

function parse(text: string) {
	try {
		return new MarkDownDOM(text);
	} catch (error) {
		const message = error.message;
		const stack = error.stack.split('\n');
		return { message, stack, toString: () => error.stack };
	}
}

function print(parts: Change[], json: boolean) {
	let html = '<ol>';
	let indent = 0;
	for (const part of parts) {
		const lines = part.value.split('\n');
		for (const line of lines) {
			if (json && line.length === 0) {
				continue;
			}

			if (json && (line.endsWith('},') || line.endsWith('}') || (!(line.endsWith('[],') || line.endsWith('[]')) && (line.endsWith('],') || line.endsWith(']'))))) {
				indent--;
			}

			const style = part.added ? 'color: green; font-weight: bold;' : (part.removed ? 'color: maroon; text-decoration: line-through;' : '');
			html += `<li><code style="${style} padding-left: ${indent}em;">${line}</code></li>`;

			if (json && (line.endsWith('{') || line.endsWith('['))) {
				indent++;
			}
		}
	}

	html += '</ol>';
	return html;
}
