# Hierarchy

Identifying possible children for all types of elements.

- paragraph block
  - run span
    - text
  - link candidate span
    - bold format span
      - italic format span
    - italic format span
      - bold format span
  - link span
    - …
- header block
- table block
- code block

To do :-)
