import BlockBase from "./BlockBase";
import HeaderSpan from "./HeaderSpan";
import LineBreak from "./LineBreak";

export default class HeaderBlock extends BlockBase {
  public static readonly type = 'header';
  public readonly type = HeaderBlock.type;

  constructor(indent?: string | null) {
    super();
    this.indent = indent || null;
    this.level = 1;
  }

  public indent: string | null = null; // TODO: get & set and check for whitespace
  public level: number; // TODO: get & set and check for range
  public readonly spans: HeaderSpan[] = [];
  public lineBreak: LineBreak | null = null;

  public toString() {
    let markdown = '';
    if (this.indent !== null) {
      markdown += this.indent;
    }

    markdown += '#'.repeat(this.level);
    for (const span of this.spans) {
      markdown += span.toString();
    }

    if (this.lineBreak !== null) {
      markdown += this.lineBreak;
    }

    return markdown;
  }
}
