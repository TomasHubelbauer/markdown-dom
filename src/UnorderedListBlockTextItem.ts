import UnorderedListBlockItemBase from "./UnorderedListBlockItemBase";
import LineBreak from "./LineBreak";

export default class UnorderedListBlockTextItem extends UnorderedListBlockItemBase {
  public static readonly type = 'text';
  public readonly type = UnorderedListBlockTextItem.type;
  public text: string | null;
  public indent: string | null = null;
  public lineBreak: LineBreak | null = null;

  constructor(text?: string | null) {
    super();
    this.text = text || null;
  }

  public toString() {
    let markdown = '';
    if (this.indent !== null) {
      markdown += this.indent;
    }

    markdown += '-';
    if (this.text !== null) {
      markdown += this.text;
    }

    if (this.lineBreak !== null) {
      markdown += this.lineBreak;
    }

    return markdown;
  }
}
