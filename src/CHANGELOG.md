# Changelog

## `0.0.9` (2018-04-23)

Implement parsing nested unordered list items and checkboxes.

## `0.0.8` (2018-04-22)

Fix parsing first header if it is non-first-level.

## `0.0.7` (2018-04-22)

Fix a problem with single-line checkbox.

## `0.0.6` (2018-04-22)

Implement parsing unordered lists and checkboxen in them.

## `0.0.5` (2018-04-22)

Fix up types temporarily.

## `0.0.4` (2018-04-21)

Fix broken publish.

## `0.0.3` (2018-04-21)

Implement rudimentary table parsing.

## `0.0.2` (2018-04-20)

Compile for the `node` WebPack `target`.

## `0.0.1` (2018-04-20)

Initial version with rudimentary block parsing and inline link parsing.
