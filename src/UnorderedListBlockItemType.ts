import UnorderedListBlockTextItem from './UnorderedListBlockTextItem';
import UnorderedListBlockCheckboxItem from './UnorderedListBlockCheckboxItem';

type UnorderedListBlockItemType = typeof UnorderedListBlockTextItem.type | typeof UnorderedListBlockCheckboxItem.type;

export default UnorderedListBlockItemType;
