import HeaderBlock from "./HeaderBlock";
import ParagraphBlock from "./ParagraphBlock";
import TableBlock from "./TableBlock";
import UnorderedListBlock from "./UnorderedListBlock";

type Block = HeaderBlock | ParagraphBlock | TableBlock | UnorderedListBlock;

export default Block;
