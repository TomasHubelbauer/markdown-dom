import HeaderBlock from "./HeaderBlock";
import ParagraphBlock from "./ParagraphBlock";
import Block from "./Block";
import UnorderedListBlock from "./UnorderedListBlock";
import UnorderedListBlockItem from "./UnorderedListBlockItem";
import BlockType from "./BlockType";
import RunSpan from "./RunSpan";
import TableBlock from "./TableBlock";
import UnorderedListBlockTextItem from "./UnorderedListBlockTextItem";
import UnorderedListBlockCheckboxItem from "./UnorderedListBlockCheckboxItem";
import LinkSpan from "./LinkSpan";
import LineBreakSpan from "./LineBreakSpan";
import ParagraphSpan from "./ParagraphSpan";
import FormatSpan from "./FormatSpan";
import LinkCandidateSpan from "./LinkCandidateSpan";
import FormatCandidateSpan from "./FormatCandidateSpan";

type BlockState =
	| { type: typeof MarkDownDOM.initiatedParsingExpectingBlockState; }
	| { type: typeof MarkDownDOM.expectedBlockParsingParagraphBlockState; }
	| { type: typeof MarkDownDOM.parsedParagraphBlockParsingParagraphBlockState; }

	| { type: typeof MarkDownDOM.bumpHeaderBlockLevelOrAppendHeaderBlockTextOrFinishHeaderBlockState; }
	| { type: typeof MarkDownDOM.appendHeaderBlockTextOrFinishHeaderBlockState; }
	| { type: typeof MarkDownDOM.disambiguateWhiteSpaceParagraphBlockState; }
	| { type: typeof MarkDownDOM.parseTableHeaderCellState; }
	| { type: typeof MarkDownDOM.parseTableBodyCellState; }
	| { type: typeof MarkDownDOM.parseUnorderedListBlockTextItemState; }
	| { type: typeof MarkDownDOM.parseUnorderedListBlockCheckboxItemState; }
	;

type InlineState =
	| { type: null }
	| { type: typeof MarkDownDOM.parsingLinkTargetState; }
	| { type: typeof MarkDownDOM.parsingLinkState; }
	| { type: typeof MarkDownDOM.parsingItalicsState; }
	| { type: typeof MarkDownDOM.parsingBoldState; }
	| { type: typeof MarkDownDOM.closingBoldState; }
	;

type Transition = (character: string) => void;

// MarkDown structure is essentially two-level, the first level are blocks (header, paragraph, list, code, table, …) and the second level are inlines (bold, italic, link, image, …).
// Not all blocks allow parsing all inlines, so we need to be aware of what block we are at when parsing inlines. So we need to represent this in the state somehow.
// There are multiple options, we could have a flat list of block+inline states (and duplicate a lot of cases), objects with type and extra metadata or this, a split between block and inline context.
type State = {
	context: 'block' | 'ínline';
	block: BlockState;
	inline: InlineState;
};

export default class MarkDownDOM {
	public readonly blocks: Block[] = [];
	private index = 0;
	private state: State = {
		context: 'block',
		block: { type: 'initiatedParsing-expectingBlock' },
		inline: { type: null },
	};

	constructor(markdown?: string) {
		if (markdown !== undefined) {
			this.parse(markdown);
		}
	}

	private parse(markdown: string) {
		for (let index = 0; index < markdown.length; index++) {
			const character = markdown[index];
			switch (this.state.context) {
				case 'block': this.getBlockStateTransition()(character); break;
				case 'inline': this.getInlineStateTransition()(character); break;
				default: {
					throw new Error(`Invalid context ${this.state.context}. Expected 'block' or 'inline'.`);
				}
			}

			this.index++;
		}

		// Cleanup
		if (this.blocks.length > 0) {
			const block = this.blocks[this.blocks.length - 1];
			if (block.type === ParagraphBlock.type) {
				if (block.spans.length === 0) {
					block.spans.push(new LineBreakSpan('\n'));
				}
			}
		}
	}

	private getBlockStateTransition(): (character: string) => void {
		switch (this.state.block.type) {
			case MarkDownDOM.initiatedParsingExpectingBlockState: return this.advanceInitiatedParsingExpectingBlockState;
			case MarkDownDOM.expectedBlockParsingParagraphBlockState: return this.advanceExpectedBlockParsingParagraphBlockStateState;
			case MarkDownDOM.parsedParagraphBlockParsingParagraphBlockState: return this.advanceParsedParagraphBlockParsingParagraphBlockState;

			case MarkDownDOM.parseUnorderedListBlockTextItemState: return this.advanceParseUnorderedBlockItemState;
			case MarkDownDOM.parseUnorderedListBlockCheckboxItemState: return this.advanceParseUnorderedListBlockCheckboxItemState;
			case MarkDownDOM.parseTableHeaderCellState: return this.advanceParseTableHeaderCellState;
			case MarkDownDOM.parseTableBodyCellState: return this.advanceParseTableBodyCellState;
			case MarkDownDOM.bumpHeaderBlockLevelOrAppendHeaderBlockTextOrFinishHeaderBlockState: return this.advanceBumpHeaderBlockLevelOrAppendHeaderBlockTextOrFinishHeaderBlockState;
			case MarkDownDOM.appendHeaderBlockTextOrFinishHeaderBlockState: return this.advanceAppendHeaderBlockTextOrFinishHeaderBlockState;
			case MarkDownDOM.disambiguateWhiteSpaceParagraphBlockState: return this.advanceDisambiguateWhiteSpaceParagraphBlockState;
		}

		throw new Error(`Invalid block state '${this.state.block! /* never */.type}' at index ${this.index}.`);
	}

	private getInlineStateTransition(): (character: string) => void {
		switch (this.state.inline.type) {
			case MarkDownDOM.parsingLinkState: return this.advanceParsingLinkState;
			case MarkDownDOM.parsingLinkTargetState: return this.advanceParsingLinkTargetState;
			case MarkDownDOM.parsingItalicsState: return this.advanceParsingItalicsState;
			case MarkDownDOM.parsingBoldState: return this.advanceParsingBoldState;
			case MarkDownDOM.closingBoldState: return this.advanceClosingBoldState;
			case null: {
				throw new Error('Asked for inline state transition when not in inline context.');
			}
		}

		throw new Error(`Invalid inline state '${this.state.inline! /* never */.type}' at index ${this.index}.`);
	}

	private getCurrentBlock() {
		const [block] = this.blocks.slice(-1);
		if (block === undefined) {
			throw new Error(`There must be a block.`);
		}

		return block;
	}

	private getCurrentParagraphBlock() {
		const block = this.getCurrentBlock();
		if (block.type !== ParagraphBlock.type) {
			throw new Error(`Block must be a paragraph block.`);
		}

		return block;
	}

	private getCurrentParagraphBlockSpan() {
		const block = this.getCurrentParagraphBlock();
		const [span] = block.spans.slice(-1);
		if (span === undefined) {
			throw new Error(`There must be a span.`);
		}

		return span;
	}

	private getCurrentParagraphBlockFormatCandidateSpan() {
		const span = this.getCurrentParagraphBlockSpan();
		if (span.type !== FormatCandidateSpan.type) {
			throw new Error(`Span must be a format candidate span.`);
		}

		return span;
	}

	public static readonly initiatedParsingExpectingBlockState = 'initiatedParsing-expectingBlock';
	private advanceInitiatedParsingExpectingBlockState: Transition = character => {
		this.blocks.push(new ParagraphBlock(new RunSpan(character)));
		this.state.block = { type: MarkDownDOM.expectedBlockParsingParagraphBlockState };
	};

	public static readonly expectedBlockParsingParagraphBlockState = 'expectedBlock-parsingParagraphBlock';
	private advanceExpectedBlockParsingParagraphBlockStateState: Transition = character => {
		const span = this.getCurrentParagraphBlockSpan();
		switch (span.type) {
			case RunSpan.type: {
				span.text += character;
				this.state.block = { type: MarkDownDOM.parsedParagraphBlockParsingParagraphBlockState };
				return;
			}
			default: {
				throw new Error(`Unexpected span type ${span.type}.`);
			}
		}

		// 	switch (span.type) {
		// 		case 'line-break': {
		// 			block.spans.push(new RunSpan(character));
		// 			return;
		// 		}
		// 		case 'link': {
		// 			block.spans.push(new RunSpan(character));
		// 			return;
		// 		}
		// 		case 'formatted': {
		// 			block.spans.push(new RunSpan(character));
		// 			return;
		// 		}
		// 	}
	};

	// TODO: Make it so that spans capable of containing more spans are set up that way (like blocks) and not for strings.
	public static readonly parsedParagraphBlockParsingParagraphBlockState = 'parsedParagraphBlock-parsingParagraphBlock';
	private advanceParsedParagraphBlockParsingParagraphBlockState: Transition = character => {
		switch (character) {
			case '[': {
				this.getCurrentParagraphBlock().spans.push(new LinkCandidateSpan());
				return;
			}
			case '*': {
				if (this.getCurrentParagraphBlockSpan().type !== FormatCandidateSpan.type) {
					this.getCurrentParagraphBlock().spans.push(new FormatCandidateSpan('italic-asterisk'));
				} else {
					// TODO: Turn into an actual format span!
					this.getCurrentParagraphBlockFormatCandidateSpan();
				}

				return;
			}
		}

		const span = this.getCurrentParagraphBlockSpan();
		switch (span.type) {
			case RunSpan.type: {
				span.text += character;
				this.state.block = { type: MarkDownDOM.parsedParagraphBlockParsingParagraphBlockState };
				return;
			}
			case LinkCandidateSpan.type: {
				span.text += character;
				return;
			}
			case FormatCandidateSpan.type: {
				span.text += character;
				return;
			}
			default: {
				throw new Error(`Unexpected span type ${span.type}.`);
			}
		}
	};



	// switch (character) {
	// 	case ' ': {
	// 		const paragraphBlock = new ParagraphBlock();
	// 		this.blocks.push(paragraphBlock);
	// 		paragraphBlock.spans.push(new RunSpan(character));
	// 		this.state.block = { type: 'disambiguateWhiteSpaceParagraphBlock' };
	// 		return;
	// 	}

	// 	case '#': {
	// 		this.blocks.push(new HeaderBlock());
	// 		this.state.block = { type: 'bumpHeaderBlockLevelOrAppendHeaderBlockTextOrFinishHeaderBlock' };
	// 		return;
	// 	}

	// 	// TODO: Change to "paragraph until proven table", do not assume table straight away.
	// 	case '|': {
	// 		const tableBlock = new TableBlock();
	// 		this.blocks.push(tableBlock);
	// 		// TODO: Do not push the empty string yet, we don't know we need it, may be end of stream
	// 		tableBlock.columns.push('');
	// 		this.state.block = { type: 'parseTableHeaderCell' };
	// 		return;
	// 	}

	// 	// TODO: Change to "paragraph until proven list", do not assume list straight away.
	// 	case '-': {
	// 		let unorderedListBlock: UnorderedListBlock;
	// 		if (this.blocks.length !== 0) {
	// 			const block = this.blocks[this.blocks.length - 1];
	// 			if (block.type !== UnorderedListBlock.type) {
	// 				unorderedListBlock = new UnorderedListBlock();
	// 				this.blocks.push(unorderedListBlock);
	// 			} else {
	// 				unorderedListBlock = block;
	// 			}
	// 		} else {
	// 			unorderedListBlock = new UnorderedListBlock();
	// 			this.blocks.push(unorderedListBlock);
	// 		}

	// 		unorderedListBlock.items.push(new UnorderedListBlockTextItem());
	// 		this.state.block = { type: 'parseUnorderedListBlockTextItem' };
	// 		return;
	// 	}

	// 	case '\n': {
	// 		this.blocks.push(new ParagraphBlock());
	// 		// TODO: More precise state (what have we finished parsing and what are we moving onto)
	// 		this.state.block = { type: 'initiatedParsing-expectingBlock' };
	// 		return;
	// 	}

	// 	// TODO: Do not be italic-happy, it could be an unordered list or just a paragraph, too, wait a bit.
	// 	case '*': {
	// 		this.state.inline = { type: 'parsingItalics' };
	// 		return;
	// 	}
	// }

	public static readonly parseUnorderedListBlockTextItemState = 'parseUnorderedListBlockTextItem';
	private advanceParseUnorderedBlockItemState: Transition = character => {
		switch (character) {
			case '\n': {
				const block = this.blocks[this.blocks.length - 1];
				if (block.type !== UnorderedListBlock.type) throw new Error('');
				if (block.items.length === 0) throw new Error('');
				const item = block.items[block.items.length - 1];
				item.lineBreak = '\n';
				// TODO: More precise state (what have we finished parsing and what are we moving onto)
				this.state.block = { type: 'initiatedParsing-expectingBlock' };
				return;
			}

			case ']': {
				// TODO: Make this actually stateful
				const block = this.blocks[this.blocks.length - 1];
				if (block.type !== UnorderedListBlock.type) throw new Error('');
				if (block.items.length === 0) throw new Error('');
				const item = block.items[block.items.length - 1];
				if (item.type === 'text') {
					if (item.text === null) {
						item.text = character;
					} else if (item.text.endsWith('[x')) {
						block.items.pop();
						block.items.push(new UnorderedListBlockCheckboxItem('x'));
						this.state.block = { type: 'parseUnorderedListBlockCheckboxItem' };
						return;
					} else if (item.text.endsWith('[X')) {
						block.items.pop();
						block.items.push(new UnorderedListBlockCheckboxItem('X'));
						this.state.block = { type: 'parseUnorderedListBlockCheckboxItem' };
						return;
					} else if (item.text.endsWith('[ ')) {
						block.items.pop();
						block.items.push(new UnorderedListBlockCheckboxItem(null));
						this.state.block = { type: 'parseUnorderedListBlockCheckboxItem' };
						return;
					} else {
						item.text += character;
					}
				}

				return;
			}
		}

		const block = this.blocks[this.blocks.length - 1];
		if (block.type !== UnorderedListBlock.type) throw new Error('');
		if (block.items.length === 0) throw new Error('');
		const item = block.items[block.items.length - 1];
		if (item.type !== UnorderedListBlockTextItem.type) throw new Error('');
		if (item.text === null) {
			item.text = character;
		} else {
			item.text += character;
		}
	};

	public static readonly parseUnorderedListBlockCheckboxItemState = 'parseUnorderedListBlockCheckboxItem';
	private advanceParseUnorderedListBlockCheckboxItemState: Transition = character => {
		if (character === '\n') {
			// TODO: More precise state (what have we finished parsing and what are we moving onto)
			this.state.block = { type: 'initiatedParsing-expectingBlock' };
			return;
		}

		if (this.blocks.length === 0) {
			throw new Error('');
		}

		const block = this.blocks[this.blocks.length - 1];
		if (block.type !== UnorderedListBlock.type) {
			throw new Error('');
		}

		if (block.items.length === 0) {
			throw new Error('');
		}

		const item = block.items[block.items.length - 1];
		if (item.type === 'checkbox') {
			item.text += character;
		}
	};

	public static readonly parseTableHeaderCellState = 'parseTableHeaderCell';
	private advanceParseTableHeaderCellState: Transition = character => {
		switch (character) {
			case '|': {
				// TODO: Not necessarily, may be followed by '\n' in which case it will be an extra phantom cell
				// TODO: Do not enter the new cell prematurely, instead do it based on the next char
				if (this.blocks.length === 0) {
					throw new Error('');
				}

				const block = this.blocks[this.blocks.length - 1];
				if (block.type !== TableBlock.type) {
					throw new Error('');
				}

				block.columns.push('');
				break;
			}

			case '\n': {
				if (this.blocks.length === 0) {
					throw new Error('');
				}

				const block = this.blocks[this.blocks.length - 1];
				if (block.type !== TableBlock.type) {
					throw new Error('');
				}

				block.rows.push([]);
				this.state.block = { type: 'parseTableBodyCell' };
				return;
			}
		}

		if (this.blocks.length === 0) {
			throw new Error('');
		}

		const block = this.blocks[this.blocks.length - 1];
		if (block.type !== TableBlock.type) {
			throw new Error('');
		}

		if (block.columns.length === 0) {
			throw new Error('');
		}

		block.columns[0] += character;
	};

	public static readonly parseTableBodyCellState = 'parseTableBodyCell';
	private advanceParseTableBodyCellState: Transition = character => {
		switch (character) {
			case '|': {
				if (this.blocks.length === 0) {
					throw new Error('');
				}

				const block = this.blocks[this.blocks.length - 1];
				if (block.type !== TableBlock.type) {
					throw new Error('');
				}

				if (block.columns.length === 0) {
					throw new Error('');
				}

				// TODO: Not yet, may be end of line
				block.columns.push('');
				return;
			}

			case '\n': {
				if (this.blocks.length === 0) {
					throw new Error('');
				}

				const block = this.blocks[this.blocks.length - 1];
				if (block.type !== TableBlock.type) {
					throw new Error('');
				}

				block.rows.push([]);
				return;
			}
		}

		if (this.blocks.length === 0) {
			throw new Error('');
		}

		const block = this.blocks[this.blocks.length - 1];
		if (block.type !== TableBlock.type) {
			throw new Error('');
		}

		if (block.rows.length === 0) {
			throw new Error('');
		}

		const row = block.rows[block.rows.length - 1];
		if (row.length === 0) {
			throw new Error('');
		}

		row[0] += character;
	};

	public static readonly bumpHeaderBlockLevelOrAppendHeaderBlockTextOrFinishHeaderBlockState = 'bumpHeaderBlockLevelOrAppendHeaderBlockTextOrFinishHeaderBlock';
	private advanceBumpHeaderBlockLevelOrAppendHeaderBlockTextOrFinishHeaderBlockState: Transition = character => {
		switch (character) {
			case '#': {
				if (this.blocks.length === 0) {
					throw new Error('');
				}

				const block = this.blocks[this.blocks.length - 1];
				if (block.type !== HeaderBlock.type) {
					throw new Error('');
				}

				block.level++;
				this.state.block = { type: 'appendHeaderBlockTextOrFinishHeaderBlock' };
				return;
			}

			case '\n': {
				// TODO: More precise state (what have we finished parsing and what are we moving onto)
				this.state.block = { type: 'initiatedParsing-expectingBlock' };
				return;
			}
		}

		if (this.blocks.length === 0) {
			throw new Error('');
		}

		const block = this.blocks[this.blocks.length - 1];
		if (block.type !== HeaderBlock.type) {
			throw new Error('');
		}

		let span = block.spans[block.spans.length - 1];
		if (span === undefined) {
			span = new RunSpan();
			block.spans.push(span);
		}

		if (span.type !== 'run') {
			throw new Error('');
		}

		if (span.text === null) {
			span.text = character;
		} else {
			span.text += character;
		}

		this.state.block = { type: 'appendHeaderBlockTextOrFinishHeaderBlock' };
	};

	public static readonly appendHeaderBlockTextOrFinishHeaderBlockState = 'appendHeaderBlockTextOrFinishHeaderBlock';
	private advanceAppendHeaderBlockTextOrFinishHeaderBlockState: Transition = character => {
		switch (character) {
			case '\n': {
				if (this.blocks.length === 0) {
					throw new Error('');
				}

				const block = this.blocks[this.blocks.length - 1];
				if (block.type !== HeaderBlock.type) {
					throw new Error('');
				}

				block.lineBreak = '\n';
				// TODO: More precise state (what have we finished parsing and what are we moving onto)
				this.state.block = { type: 'initiatedParsing-expectingBlock' };
				break;
			}
			case '(': {
				if (this.blocks.length === 0) {
					throw new Error('');
				}

				const block = this.blocks[this.blocks.length - 1];
				if (block.type !== HeaderBlock.type) {
					throw new Error('');
				}

				if (block.spans.length === 0) {
					throw new Error('');
				}

				const span = block.spans[block.spans.length - 1];
				if (span.type !== 'run') {
					throw new Error('');
				}

				if (span.text === null) {
					throw new Error('');
				}

				if (span.text.endsWith(']')) {
					this.state.inline = { type: 'parsingLink' };
					this.index = this.index - 1; // TODO: Get rid of this repeat hack somehow
					break;
				}

				break;
			}
		}

		if (this.blocks.length === 0) {
			throw new Error('');
		}

		const block = this.blocks[this.blocks.length - 1];
		if (block.type !== HeaderBlock.type) {
			throw new Error('');
		}

		const span = block.spans[block.spans.length - 1];
		if (span === undefined) {
			block.spans.push(new RunSpan(character));
			return;
		}

		if (span.type !== 'run') {
			throw new Error('');
		}

		span.text += character;
	};

	public static readonly disambiguateWhiteSpaceParagraphBlockState = 'disambiguateWhiteSpaceParagraphBlock';
	private advanceDisambiguateWhiteSpaceParagraphBlockState: Transition = character => {
		switch (character) {
			case ' ': {
				if (this.blocks.length === 0) throw new Error('');
				const block = this.blocks[this.blocks.length - 1];
				if (block.type !== ParagraphBlock.type) throw new Error('');
				if (block.spans.length === 0) throw new Error('');
				const span = block.spans[block.spans.length - 1];
				if (span.type !== 'run') throw new Error('');
				span.text += character;
				break;
			}

			case '\t': {
				throw new Error('todo');
			}

			case '#': {
				if (this.blocks.length === 0) throw new Error('');
				const block = this.blocks[this.blocks.length - 1];
				if (block.type !== ParagraphBlock.type) throw new Error('');
				if (block.spans.length !== 1) throw new Error('Paragraph block is supposed to be ambiguous, but has multiple spans!');
				const span = block.spans[block.spans.length - 1];
				if (span === undefined || span.type !== RunSpan.type) throw new Error('');
				if (span.text === null) throw new Error('');
				if (!span.isWhitespace) throw new Error('Run span is supposed to be ambiguous, but has non-whitespace characters!');
				this.blocks.pop(); // Remove ambiguous paragraph.
				this.blocks.push(new HeaderBlock(span.text));
				this.state.block = { type: 'bumpHeaderBlockLevelOrAppendHeaderBlockTextOrFinishHeaderBlock' };
				return;
			}

			case '-': {
				if (this.blocks.length === 0) throw new Error('');
				const block = this.blocks[this.blocks.length - 1];
				if (block.type !== ParagraphBlock.type) throw new Error('');
				if (block.spans.length === 1) {
					const span = block.spans[0];
					if (span.type === 'run') {
						if (span.isWhitespace) {
							this.blocks.pop(); // Pop the paragraph
							let unorderedListBlock;
							if (this.blocks.length > 0) {
								const block = this.blocks[this.blocks.length - 1];
								if (block.type !== 'unordered-list') {
									if (block.type === 'paragraph' && block.spans.length === 0) {
										this.blocks.pop();
									}

									unorderedListBlock = new UnorderedListBlock();
									this.blocks.push(unorderedListBlock);
								} else {
									unorderedListBlock = block;
								}
							} else {
								unorderedListBlock = new UnorderedListBlock();
								this.blocks.push(unorderedListBlock);
							}

							unorderedListBlock.items.push(new UnorderedListBlockTextItem(span.text));
							this.state.block = { type: 'parseUnorderedListBlockTextItem' };
						}
					}
				}

				break;
			}
		}

		throw new Error('');
	};

	// public static readonly advanceParagraphBlockState = 'advanceParagraphBlock';
	// private advanceAdvanceParagraphBlockState: Transition = character => {
	// 	switch (character) {
	// 		case '\r': {
	// 			throw new Error('\\r');
	// 		}

	// 		case '\n': {
	// 			const block = this.blocks[this.blocks.length - 1];
	// 			if (block.type !== ParagraphBlock.type) throw new Error('');
	// 			if (block.spans.length === 0) throw new Error('');
	// 			const span = block.spans[block.spans.length - 1];
	// 			switch (span.type) {
	// 				case 'line-break': {
	// 					block.lineBreak = '\n';
	// 					// TODO: More precise state (what have we finished parsing and what are we moving onto)
	// 					this.state.block = { type: 'initiatedParsing-expectingBlock' };
	// 					return;
	// 				}

	// 				case 'formatted':
	// 				case 'run': {
	// 					block.spans.push(new LineBreakSpan('\n'));
	// 					return;
	// 				}

	// 				case 'link': {
	// 					throw new Error('What do?');
	// 				}
	// 			}

	// 			throw new Error(`Unexpected span type '${(span /* never */ as ParagraphSpan).type}'`);
	// 		}

	// 		case '*': {
	// 			this.state.inline = { type: 'parsingItalics' };
	// 			return;
	// 		}

	// 		case '[': {
	// 			// [`code` and *italic*]( would cause ( to look for last span which is only the FormattedSpan for italic
	// 			// TODO: Instead, on each [, keep a link candidated position (on state probably), reset on next [ or line break and consult on ]
	// 			throw new Error('TODO: Redo links!');
	// 		}

	// 		case '(': {
	// 			const block = this.blocks[this.blocks.length - 1];
	// 			if (block.type !== ParagraphBlock.type) throw new Error('');
	// 			if (block.spans.length === 0) throw new Error('');
	// 			const span = block.spans[block.spans.length - 1];
	// 			if (span.type !== RunSpan.type) throw new Error('');
	// 			if (span.text === null) throw new Error('');
	// 			if (span.text.endsWith(']')) {
	// 				this.state.inline = { type: 'parsingLink' };
	// 				this.index = this.index - 1; // Repeat
	// 				break;
	// 			}

	// 			break;
	// 		}
	// 	}
	// };

	public static readonly parsingLinkState = 'parsingLink';
	private advanceParsingLinkState: Transition = character => {
		// TODO: Looks ugly with all the switches, split into two states?

		if (this.blocks.length === 0) {
			throw new Error('');
		}

		const block = this.blocks[this.blocks.length - 1];
		let span;
		// TODO: Switch blockState type to find out if we are parsing a paragraph or a list or what
		switch (this.state.block.type) {
			case 'paragraph': {
				if (block.type !== ParagraphBlock.type) {
					throw new Error('');
				}

				if (block.spans.length === 0) {
					throw new Error('');
				}

				const lastSpan = block.spans[block.spans.length - 1];
				if (lastSpan.type !== RunSpan.type) {
					throw new Error('');
				}

				span = lastSpan;
				break;
			}
			case 'header': {
				if (block.type !== HeaderBlock.type) {
					throw new Error('');
				}

				if (block.spans.length === 0) {
					throw new Error('');
				}

				const lastSpan = block.spans[block.spans.length - 1];
				if (lastSpan.type !== RunSpan.type) {
					throw new Error('');
				}

				span = lastSpan;
				break;
			}
			default: {
				throw new Error('');
			}
		}

		if (span.text === null) {
			throw new Error('');
		}

		const index = span.text.lastIndexOf('[');
		const title = span.text.substring(index + 1, span.text.length - 1);
		span.text = span.text.substring(0, index);
		if (span.text === '') {
			switch (block.type) {
				case 'paragraph': block.spans.pop(); return;
				case 'header': block.spans.pop(); return;
			}

			throw new Error('');
		}

		// TODO: Not yet, this creates an empty link right after ]( which when serialized has the ) after URL
		switch (block.type) {
			case 'paragraph': {
				block.spans.push(new LinkSpan(title));
				this.state.inline = { type: 'parsingLinkTarget' };
				return;
			}
			case 'header': {
				block.spans.push(new LinkSpan(title));
				this.state.inline = { type: 'parsingLinkTarget' };
				return;
			}
		}

		throw new Error('');
	};

	public static readonly parsingLinkTargetState = 'parsingLinkTarget';
	private advanceParsingLinkTargetState: Transition = character => {
		switch (character) {
			case '(': break;
			case ')': {
				// TODO: Switch blockState type to find out if we are parsing a paragraph or a list or what
				switch (this.state.block.type) {
					case 'paragraph': this.state.block = { type: 'advanceParagraphBlock' }; return;
					case 'header': this.state.block = { type: 'appendHeaderBlockTextOrFinishHeaderBlock' }; return;
				}

				throw new Error('');
			}
		}

		// TODO: Switch blockState type to find out if we are parsing a paragraph or a list or what
		switch (this.state.block.type) {
			// TODO: Introduce a field above and split cases and assign to it so we can ditch the cast.
			case 'paragraph':
			case 'header': {
				if (this.blocks.length === 0) throw new Error('');
				const block = this.blocks[this.blocks.length - 1] as (HeaderBlock | ParagraphBlock);
				const span = block.spans[block.spans.length - 1];
				if (span.type !== 'link') throw new Error('');
				if (span.url === null) {
					span.url = character;
				} else {
					span.url += character;
				}

				return;
			}
		}

		throw new Error('');
	};

	public static readonly parsingItalicsState = 'parsingItalics';
	private advanceParsingItalicsState: Transition = character => {
		if (character === '*') {
			if (this.blocks.length === 0) {
				this.state.inline = { type: 'parsingBold' };
				return;
			}

			const block = this.blocks[this.blocks.length - 1];
			if (block.type !== ParagraphBlock.type) throw new Error('');
			if (block.spans.length === 0) throw new Error('');
			const span = block.spans[block.spans.length - 1];
			if (span.type === 'formatted' && span.format === 'italics') {
				this.state.block = { type: 'advanceParagraphBlock' };
			} else {
				this.state.inline = { type: 'parsingBold' };
			}
		}

		if (this.blocks.length === 0) {
			throw new Error('');
		}

		const block = this.blocks[this.blocks.length - 1];
		if (block.type !== ParagraphBlock.type) {
			throw new Error('');
		}

		if (block.spans.length === 0) {
			throw new Error('');
		}

		const span = block.spans[block.spans.length - 1];
		if (span.type !== 'formatted') {
			block.spans.push(new FormattedSpan('italics', character));
		} else {
			span.text += character;
		}
	};

	public static readonly parsingBoldState = 'parsingBold';
	private advanceParsingBoldState: Transition = character => {
		if (character === '*') {
			this.state.inline = { type: 'closingBold' };
			return;
		}

		let block = this.blocks[this.blocks.length - 1];
		if (block === undefined) {
			block = new ParagraphBlock();
			this.blocks.push(block);
		} else {
			if (block.type !== ParagraphBlock.type) {
				throw new Error('');
			}
		}

		let span = block.spans[block.spans.length - 1];
		if (span === undefined) {
			span = new FormattedSpan('bold', character);
			block.spans.push(span);
			return;
		}

		if (span.type !== 'formatted') {
			throw new Error('');
		}

		span.text! += character;
	};

	public static readonly closingBoldState = 'closingBold';
	private advanceClosingBoldState: Transition = character => {
		if (character === '*') {
			//this.state.block = { type: 'advanceParagraphBlock' };
			return;
		}

		throw new Error('');
	};

	public toString() {
		let markdown = '';
		for (const block of this.blocks) {
			markdown += block.toString();
		}

		return markdown;
	}
}
