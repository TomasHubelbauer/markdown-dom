import SpanBase from "./SpanBase";

export default class LinkSpan extends SpanBase {
  public static readonly type = 'link';
  public readonly type = LinkSpan.type;
  public text: string | null = null;
  public url: string | null = null;

  constructor(text?: string | null) {
    super();
    this.text = text || null;
  }

  public toString() {
    return `[${this.text}](${this.url})`;
  }
}
