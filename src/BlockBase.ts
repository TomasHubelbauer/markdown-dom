import BlockType from "./BlockType";

export default abstract class BlockBase {
  public abstract readonly type: BlockType;
  public abstract toString(): string;
}
