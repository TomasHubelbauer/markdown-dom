import BlockBase from "./BlockBase";
import UnorderedListBlockItem from "./UnorderedListBlockItem";

export default class UnorderedListBlock extends BlockBase {
  public static readonly type = 'unordered-list';
  public readonly type = UnorderedListBlock.type;
  public readonly items: UnorderedListBlockItem[] = [];

  public toString() {
    let markdown = '';
    for (const item of this.items) {
      markdown += item.toString();
    }

    return markdown;
  }
}
