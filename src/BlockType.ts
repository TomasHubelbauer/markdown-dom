import HeaderBlock from "./HeaderBlock";
import ParagraphBlock from "./ParagraphBlock";
import TableBlock from "./TableBlock";
import UnorderedListBlock from "./UnorderedListBlock";

type BlockType =
  | typeof HeaderBlock.type
  | typeof ParagraphBlock.type
  | typeof TableBlock.type
  | typeof UnorderedListBlock.type
  ;

export default BlockType;
