import BlockBase from "./BlockBase";
import ParagraphSpan from "./ParagraphSpan";
import LineBreak from "./LineBreak";

export default class ParagraphBlock extends BlockBase {
  public static readonly type = 'paragraph';
  public readonly type = ParagraphBlock.type;
  public readonly spans: ParagraphSpan[] = [];
  public lineBreak: LineBreak | null = null;

  constructor(...spans: ParagraphSpan[]) {
    super();
    this.spans = spans;
  }

  public toString() {
    let markdown = '';
    for (const span of this.spans) {
      markdown += span.toString();
    }

    // TODO: Fix this so it inserts a line-break span properly.
    if (this.spans.length === 0) {
      markdown += '\n';
    }

    if (this.lineBreak !== null) {
      markdown += this.lineBreak;
    }

    return markdown;
  }
}
