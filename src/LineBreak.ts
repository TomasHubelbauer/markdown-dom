type LineBreak = '\n' | '\r\n';

export default LineBreak;
