import SpanType from "./SpanType";

export default abstract class SpanBase {
  public abstract readonly type: SpanType;
}
