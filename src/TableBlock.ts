import BlockBase from "./BlockBase";
import TableColumn from "./TableColumn";
import TableRow from "./TableRow";

export default class TableBlock extends BlockBase {
  public static readonly type = 'table';
  public readonly type = TableBlock.type;

  public readonly columns: TableColumn[] = [];
  public readonly rows: TableRow[][] = [];

  public toString() {
    return 'todo: table\n';
  }
}
