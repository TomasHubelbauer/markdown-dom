import SpanBase from "./SpanBase";
import LineBreak from "./LineBreak";

export default class LineBreakSpan extends SpanBase {
  public static readonly type = 'line-break';
  public readonly type = LineBreakSpan.type;
  public lineBreak: LineBreak;

  constructor(lineBreak: LineBreak) {
    super();
    this.lineBreak = lineBreak;
  }

  public toString() {
    return this.lineBreak;
  }
}
