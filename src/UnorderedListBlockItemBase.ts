import UnorderedListBlockItemType from "./UnorderedListBlockItemType";

export default abstract class UnorderedListBlockItemBase {
  public abstract readonly type: UnorderedListBlockItemType;
  public abstract toString(): string;
}
