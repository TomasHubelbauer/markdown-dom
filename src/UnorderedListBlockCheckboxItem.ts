import UnorderedListBlockItemBase from "./UnorderedListBlockItemBase";
import LineBreak from "./LineBreak";

export default class UnorderedListBlockCheckboxItem extends UnorderedListBlockItemBase {
  public static readonly type = 'checkbox';
  public readonly type = UnorderedListBlockCheckboxItem.type;
  public text: string | null = null;
  public readonly check: 'x' | 'X' | null; // TODO: get & set to check values
  public indent: string | null = null;
  public checkIndent: string | null = null;
  public lineBreak: LineBreak | null = null;

  constructor(check: 'x' | 'X' | null) {
    super();
    this.check = check;
  }

  public toString() {
    return `${this.indent}-${this.checkIndent}[${this.check || ' '}]${this.text}${this.lineBreak}`;
  }
}
