import UnorderedListBlockTextItem from "./UnorderedListBlockTextItem";
import UnorderedListBlockCheckboxItem from "./UnorderedListBlockCheckboxItem";

type UnorderedListBlockItem = UnorderedListBlockTextItem | UnorderedListBlockCheckboxItem;

export default UnorderedListBlockItem;
