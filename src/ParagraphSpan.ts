import RunSpan from "./RunSpan";
import LinkSpan from "./LinkSpan";
import LinkCandidateSpan from "./LinkCandidateSpan";
import LineBreakSpan from "./LineBreakSpan";
import FormatSpan from "./FormatSpan";
import FormatCandidateSpan from "./FormatCandidateSpan";

type ParagraphSpan =
  | RunSpan
  | LinkSpan
  | LinkCandidateSpan
  | LineBreakSpan
  | FormatSpan
  | FormatCandidateSpan
  ;

export default ParagraphSpan;
