import SpanBase from "./SpanBase";

export default class FormatCandidateSpan extends SpanBase {
  public static readonly type = 'format-candidate';
  public readonly type = FormatCandidateSpan.type;
  public text: string = '';
  public format: Format;

  constructor(format: Format, text?: string) {
    super();
    this.format = format;
    if (text !== undefined) {
      this.text = text;
    }
  }

  public toString() {
    switch (this.format) {
      case 'italic-asterisk': return '*' + this.text;
      case 'italic-underscore': return '_' + this.text;
      case 'bold-asterisks': return '**' + this.text;
      case 'bold-underscores': return '__' + this.text;
      default: throw new Error(`Unexpected format ${this.format}`);
    }
  }
}
