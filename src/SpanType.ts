import RunSpan from "./RunSpan";
import LinkSpan from "./LinkSpan";
import LinkCandidateSpan from "./LinkCandidateSpan";
import LineBreakSpan from "./LineBreakSpan";
import FormatSpan from "./FormatSpan";
import FormatCandidateSpan from "./FormatCandidateSpan";

type SpanType =
  | typeof RunSpan.type
  | typeof LinkSpan.type
  | typeof LinkCandidateSpan.type
  | typeof LineBreakSpan.type
  | typeof FormatSpan.type
  | typeof FormatCandidateSpan.type
  ;

export default SpanType;
