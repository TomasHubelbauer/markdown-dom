import TableCell from "./TableCell";

type TableRow = Readonly<TableCell>;

export default TableRow;
