import SpanBase from "./SpanBase";

export default class FormatSpan extends SpanBase {
  public static readonly type = 'format';
  public readonly type = FormatSpan.type;
  public text: string = '';
  public format: 'bold' | 'italics';

  constructor(format: 'bold' | 'italics', text?: string) {
    super();
    this.format = format;
    if (text !== undefined) {
      this.text = text;
    }
  }

  public toString() {
    switch (this.format) {
      case 'bold': return `**${this.text}**`;
      case 'italics': return `*${this.text}*`;
      default: throw new Error(`Unexpected format ${this.format}`);
    }
  }
}
