import SpanBase from "./SpanBase";

export default class RunSpan extends SpanBase {
  public static readonly type = 'run';
  public readonly type = RunSpan.type;
  public text: string = '';

  constructor(text?: string) {
    super();
    if (text !== undefined) {
      this.text = text;
    }
  }

  public get isWhitespace() {
    return /^\s+$/.test(this.text);
  }

  public toString() {
    return this.text;
  }
}
