import SpanBase from "./SpanBase";

export default class LinkCandidateSpan extends SpanBase {
  public static readonly type = 'link-candidate';
  public readonly type = LinkCandidateSpan.type;
  public text: string = '';

  constructor(text?: string) {
    super();
    if (text !== undefined) {
      this.text = text;
    }
  }

  public toString() {
    return `[${this.text}`;
  }
}
