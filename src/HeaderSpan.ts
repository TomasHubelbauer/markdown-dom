import RunSpan from "./RunSpan";
import LinkSpan from "./LinkSpan";

type HeaderSpan = RunSpan | LinkSpan;

export default HeaderSpan;
